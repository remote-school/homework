defmodule HomeworkWeb.Router do
  use HomeworkWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", HomeworkWeb do
    pipe_through :api
  end
end
