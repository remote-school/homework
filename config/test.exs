import Config

database_url = System.get_env("DATABASE_URL") || "localhost"

# Configure your database
config :homework, Homework.Repo,
  username: "postgres",
  password: "postgres",
  database: "homework_test",
  hostname: database_url,
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :homework_web, HomeworkWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
