use Mix.Config

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

pool_size = System.get_env("POOL_SIZE") || "10"

config :homework, Homework.Repo,
  # ssl: true,
  url: database_url,
  pool_size: String.to_integer(pool_size)

hostname =
  System.get_env("__HOSTNAME__") ||
    raise """
    environment variable __HOSTNAME__ is missing.
    """

port = System.get_env("PORT") || "4000"

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :homework_web, HomeworkWeb.Endpoint,
  http: [:inet6, port: String.to_integer(port)],
  url: [host: hostname, port: port],
  secret_key_base: secret_key_base,
  server: true

config :cors_plug,
  origin: [System.fetch_env!("CORS_ORIGINS")],
  methods: [
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
    "CONNECT",
    "OPTIONS",
    "TRACE",
    "PATCH"
  ]
