# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
import Config

# Configure Mix tasks and generators
config :homework,
  ecto_repos: [Homework.Repo]

config :homework_web,
  ecto_repos: [Homework.Repo],
  generators: [context_app: :homework]

# Configures the endpoint
config :homework_web, HomeworkWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "2Fs5QCTaY02aW9BXrDFJ0fy1Fa6bzEWGThYvUZ2FmKgrn8OH9ihXqh1WFGOLByV8",
  render_errors: [view: HomeworkWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: HomeworkWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
