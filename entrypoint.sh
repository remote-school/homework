#!/usr/bin/env sh

NAME="Homework"
BIN="homework"

echo "Running $NAME migrations..."
$BIN eval "$NAME.Release.migrate"
echo ""

echo "Starting $NAME app..."
$BIN start
