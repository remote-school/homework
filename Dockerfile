###################
### Build stage ###
###################

FROM elixir:1.9.0-alpine as build

# Set up build dependencies
RUN apk add git build-base nodejs npm yarn python && \
  mix local.hex --force && \
  mix local.rebar --force

ARG MIX_ENV=prod
WORKDIR /usr/local/src/homework

# install mix dependencies
COPY apps/homework/mix.exs apps/homework/mix.exs
COPY apps/homework_web/mix.exs apps/homework_web/mix.exs
COPY mix.exs mix.lock ./
COPY config config
RUN mix do deps.get, compile

# build project
COPY apps/homework/priv apps/homework/priv
COPY apps/homework_web/priv apps/homework_web/priv
COPY apps/homework/lib apps/homework/lib
COPY apps/homework_web/lib apps/homework_web/lib
RUN mix compile

# build release
RUN mix release


#####################
### Release image ###
#####################

FROM alpine:3.9
RUN apk add --update bash libressl fish && \
  mkdir -p /usr/local/src/homework
WORKDIR /usr/local/src/homework
COPY --from=build /usr/local/src/homework/_build/prod/rel/homework ./
COPY entrypoint.sh /usr/local/src/homework/
RUN chown -R nobody: /usr/local/src/homework && \
  chmod +x /usr/local/src/homework/entrypoint.sh && \
  ln -s /usr/local/src/homework/bin/homework /usr/local/bin/homework

USER nobody
EXPOSE 4000
ENTRYPOINT ["/usr/local/src/homework/entrypoint.sh" ]
CMD ["foreground"]
