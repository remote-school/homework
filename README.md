# homework

Homework is a microservice that lets Teachers organize Classes' Areas, subdividing them into weekly Homeworks to do (for Students) or to correct (for Teachers).

## Parameters

You need these environment variables:

| Variable                | type                              | default | howto                                                      |
| ----------------------- | --------------------------------- | ------- | ---------------------------------------------------------- |
| `__HOSTNAME__`          | string                            | ""      |                                                            |
| `CORS_ORIGIN`           | string                            | ""      |                                                            |
| `SECRET_KEY_BASE`       | string                            | ""      | `mix phx.gen.secret`                                       |
| `DATABASE_URL`          | string                            | ""      | e.g. `ecto://postgres:postgres@database:5432/homework_dev` |
| `PORT`                  | string                            | ""      | e.g. `4000`                                                |
| `MIX_ENV`               | string                            | ""      | e.g. `prod`                                                |
| `POOL_SIZE`             | string                            | ""      | e.g. `10`                                                  |
| `CI`                    | 1 for CI environment, 0 otherwise | ""      | e.g. `1`                                                   |
| `AWS_ACCESS_KEY_ID`     | string of AWS setting             | ""      |                                                            |
| `AWS_SECRET_ACCESS_KEY` | string of AWS setting             | ""      |                                                            |
| `AWS_REGION`            | string of AWS setting             | ""      |                                                            |
| `AWS_BASE_URL`          | string of AWS setting             | ""      |                                                            |
| `AWS_S3_BUCKET`         | string of AWS setting             | ""      |                                                            |

## Run with docker-compose

Pre-requisite: create a `.docker-env` file with the necessary environment variables.

```bash
docker-compose build
docker-compose up
```
